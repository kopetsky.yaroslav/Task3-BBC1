﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjects;

namespace BLL1
{
    public class BLL
    {
        IWebDriver driver = new ChromeDriver();
        public string SubmitRequestBBC()
        {
            Dictionary<string, string> testvalues = new Dictionary<string, string>();
            testvalues.Add("SAMPLE", "SAMPLE");

            driver.Navigate().GoToUrl("https://www.bbc.com/news/10725415");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            FormPage homePage = new FormPage(driver);
            FormComplexObject fco = new FormComplexObject();
            fco.FillForm(testvalues, homePage);
            fco.PressSumbitButton(homePage);
            string s = homePage.GetTextXpath("button", 0);

            return homePage.ErrorMessage.Text;

        }

        public void LoremIpsumMainPage()
        {
           

            driver.Navigate().GoToUrl("https://lipsum.com/");
            

        }

        public void ClickLanguageButton()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
            LoremIpsumMainPagecs LoremIpsum = new LoremIpsumMainPagecs(driver);
            LoremIpsum.russianLanguageButton.Click();


           
        }
        public bool ContainsWord(string word)
        {
            LoremIpsumMainPagecs LoremIpsum = new LoremIpsumMainPagecs(driver);
            


            return LoremIpsum.textParagraph.Text.Contains(word);
        }

        public void generateWords()
        {
            LoremIpsumMainPagecs LoremIpsum = new LoremIpsumMainPagecs(driver);



            LoremIpsum.generateButton.Click();
        }
        public bool ContainsWordParagraph(string word)
        {
            LoremIpsumMainPagecs LoremIpsum = new LoremIpsumMainPagecs(driver);



            return LoremIpsum.firstParagraph.Text.Contains(word);
        }

    }
}