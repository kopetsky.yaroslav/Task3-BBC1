using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjects;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowProject2.StepDefinitions
{
    [Binding]
    public class Feature2StepDefinitions
    {

        IWebDriver driver = new ChromeDriver();
        Dictionary<string, string> testvalues = new Dictionary<string, string>();
        string ErrorMessageText;
         [Given(@"\[bbc form page]")]
        public void GivenBbcFormPage()
        {
           

            driver.Navigate().GoToUrl("https://www.bbc.com/news/10725415");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
          
        }

        [When(@"\[clicked on send button]")]
        public void WhenClickedOnSendButton()
        {
            testvalues.Add("SAMPLE", "SAMPLE");
            FormPage homePage = new FormPage(driver);
            FormComplexObject fco = new FormComplexObject();
            fco.FillForm(testvalues, homePage);
            fco.PressSumbitButton(homePage);
            ErrorMessageText = homePage.ErrorMessage.Text;
        }

        [Then(@"\[error message shown]")]
        public void ThenErrorMessageShown()
        {
            Assert.AreEqual("Name can't be blank", ErrorMessageText);
        }
    }
}
