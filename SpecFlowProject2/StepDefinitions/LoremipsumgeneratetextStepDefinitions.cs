using BLL1;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowProject2.StepDefinitions
{
    [Binding]
    public class LoremipsumgeneratetextStepDefinitions
    {
        BLL bll = new BLL();
        [Given(@"\[main page]")]
        public void GivenMainPage()
        {
            bll.LoremIpsumMainPage();
        }

        [When(@"\[click genrate text]")]
        public void WhenClickGenrateText()
        {
            bll.generateWords();
        }

        [Then(@"\[compare text]")]
        public void ThenCompareText()
        {
            Assert.AreEqual(bll.ContainsWordParagraph("Lorem ipsum dolor sit amet, consectetur adipiscing elit").ToString(), "True"); 
        }


      

    }
}
