using BLL1;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowProject2.StepDefinitions
{
    [Binding]
    public class LoremIpsumStepDefinitions
    {
        BLL bll = new BLL();

        [Given(@"\[GO to loremipsum main page]")]
        public void GivenGOToLoremipsumMainPage()
        {
            bll.LoremIpsumMainPage();
        }

        [Then(@"\[click russian language button]")]
        public void ThenClickRussianLanguageButton()
        {
            bll.ClickLanguageButton();
        }

        [Then(@"\[check for word]")]
        public void ThenCheckForWord()
        {
            Assert.AreEqual(bll.ContainsWord("����").ToString(), "True");
        }



    }
}
