﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjects
{
    public class FormComplexObject
    {
        /// <summary>
        /// Inputs text from dictionary into textarea.
        /// </summary>
        /// <param name="values">Dictionary with imput values</param>
        /// <param name="_form">Page Object of SubmitRequesForm Page</param>
        public void FillForm(Dictionary<string, string> values, FormPage _form)
        {
            _form.TextArea.Click();
            foreach (string s in values.Values)
            {
                _form.TextArea.SendKeys(s);
            }


        }
        /// <summary>
        /// Clicks Submit Button
        /// </summary>
        /// <param name="_form">Page Object of SubmitRequesForm Page</param>
        public void PressSumbitButton(FormPage _form)
        {
            _form.SubmitButton.Click();
        }
    }
}
