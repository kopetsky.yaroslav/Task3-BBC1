﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjects
{
    public class FormPage
    {
        IWebDriver _driver;
        public FormPage(IWebDriver driver)
        {
            _driver = driver;
        }
       public  string GetTextXpath(String elementLabel, int elementNumber)
        {
            IWebElement el = _driver.FindElements(By.ClassName(elementLabel))[elementNumber];
            return el.GetAttribute("XPath");
        }


        public IWebElement SubmitButton => _driver.FindElement(By.XPath("/html/body/div[2]/div/main/div[5]/div/div[1]/article/div[37]/div/div/div/div/div[1]/div[7]/button"));
        public IWebElement TextArea => _driver.FindElement(By.XPath("/html/body/div[2]/div/main/div[5]/div/div[1]/article/div[37]/div/div/div/div/div[1]/div[1]/textarea"));
        public IWebElement ErrorMessage => _driver.FindElement(By.XPath("/html/body/div[2]/div/main/div[5]/div/div[1]/article/div[37]/div/div/div/div/div[1]/div[3]/div[1]/div/div"));
        
    }
}
