﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjects
{
    public class HomePage
    {
        IWebDriver _driver;
        public HomePage(IWebDriver driver)
        {
            _driver = driver;
        }

      

        IWebElement newsButtonElement => _driver.FindElement(By.XPath("/html/body/div[7]/header/div/div[1]/nav[2]/ul/li[2]/a/span"));
        IWebElement HeadlineNewsText => _driver.FindElement(By.XPath("/html/body/div[8]/div/div/div[4]/div[2]/div/div/div/div/div[1]/div/div/div[1]/div/a/h3"));

    }
}
