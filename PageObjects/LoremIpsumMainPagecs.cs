﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjects
{
    public class LoremIpsumMainPagecs
    {
        IWebDriver _driver;
        public LoremIpsumMainPagecs(IWebDriver driver)
        {
            _driver = driver;
        }



       public IWebElement russianLanguageButton => _driver.FindElement(By.XPath("/html/body/div/div[2]/div[1]/a[30]"));
        public IWebElement textParagraph => _driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[3]/div[1]/p"));
        public IWebElement generateButton => _driver.FindElement(By.XPath("/html/body/div/div[2]/div[2]/div[3]/div[4]/form/table/tbody/tr[2]/td[2]/input"));

        public IWebElement firstParagraph => _driver.FindElement(By.XPath("/html/body/div/div[2]/div[2]/div[3]/div[1]/p[1]"));
       
    }
}
